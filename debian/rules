#!/usr/bin/make -f

export DH_GOPKG := github.com/opencontainers/runc
export DH_GOLANG_INSTALL_EXTRA := libcontainer/seccomp/fixtures
export DH_GOLANG_EXCLUDES := libcontainer/integration

TAGS := apparmor seccomp

%:
	dh $@ --buildsystem=golang --with=golang --builddirectory=_build

override_dh_clean:
	dh_clean
	## Remove Files-Excluded (when built from checkout or non-DFSG tarball):
	$(RM) -rv `perl -0nE 'say $$1 if m{^Files\-Excluded\:\s*(.*?)(?:\n\n|Files:|Comment:)}sm;' debian/copyright`

override_dh_auto_configure:
	$(MAKE) -C libcontainer/criurpc
	cd man && ./md2man-all.sh
	rename 's/runc/docker-runc/' man/man8/*.8
	dh_auto_configure
	## Sub-vendor intentionally-vendored deps:
	set -ex; for pkg in \
		github.com/docker/docker/pkg \
		github.com/opencontainers/runtime-spec \
	; do \
		[ -d "Godeps/_workspace/src/$$pkg" ]; \
		mkdir -p "_build/src/$(DH_GOPKG)/vendor/$$pkg"; \
		parentDir="$$(dirname "_build/src/$(DH_GOPKG)/vendor/$$pkg")"; \
		cp -vr "Godeps/_workspace/src/$$pkg" "$$parentDir"; \
	done

override_dh_auto_build:
	dh_auto_build -- -tags '$(TAGS)'

override_dh_auto_test:
	-dh_auto_test -- -tags '$(TAGS)' -test.short
