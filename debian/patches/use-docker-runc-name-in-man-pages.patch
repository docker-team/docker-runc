From b15fe633676c8685fe4de353e49ae6a4a2a23d17 Mon Sep 17 00:00:00 2001
From: Balint Reczey <balint.reczey@canonical.com>
Date: Sat, 30 Sep 2017 00:57:23 -0400
Subject: [PATCH] Use docker-runc name in man pages

---
 man/README.md            |  4 ++--
 man/runc-checkpoint.8.md |  4 ++--
 man/runc-create.8.md     |  6 +++---
 man/runc-delete.8.md     | 10 +++++-----
 man/runc-events.8.md     |  4 ++--
 man/runc-exec.8.md       |  6 +++---
 man/runc-kill.8.md       |  6 +++---
 man/runc-list.8.md       |  8 ++++----
 man/runc-pause.8.md      |  6 +++---
 man/runc-ps.8.md         |  6 +++---
 man/runc-restore.8.md    |  6 +++---
 man/runc-resume.8.md     |  6 +++---
 man/runc-run.8.md        |  6 +++---
 man/runc-spec.8.md       | 14 +++++++-------
 man/runc-start.8.md      |  4 ++--
 man/runc-state.8.md      |  4 ++--
 man/runc-update.8.md     |  4 ++--
 man/runc.8.md            | 14 +++++++-------
 18 files changed, 59 insertions(+), 59 deletions(-)

diff --git a/man/README.md b/man/README.md
index 1d7a54f..2c4c79a 100644
--- a/man/README.md
+++ b/man/README.md
@@ -1,7 +1,7 @@
-runc man pages
+docker-runc man pages
 ====================
 
-This directory contains man pages for runc in markdown format.
+This directory contains man pages for docker-runc in markdown format.
 
 To generate man pages from it, use this command
 
diff --git a/man/runc-checkpoint.8.md b/man/runc-checkpoint.8.md
index 632dcab..5bf5494 100644
--- a/man/runc-checkpoint.8.md
+++ b/man/runc-checkpoint.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc checkpoint - checkpoint a running container
+   docker-runc checkpoint - checkpoint a running container
 
 # SYNOPSIS
-   runc checkpoint [command options] <container-id>
+   docker-runc checkpoint [command options] <container-id>
 
 Where "<container-id>" is the name for the instance of the container to be
 checkpointed.
diff --git a/man/runc-create.8.md b/man/runc-create.8.md
index c70217b..767fc7f 100644
--- a/man/runc-create.8.md
+++ b/man/runc-create.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc create - create a container
+   docker-runc create - create a container
 
 # SYNOPSIS
-   runc create [command options] <container-id>
+   docker-runc create [command options] <container-id>
 
 Where "<container-id>" is your name for the instance of the container that you
 are starting. The name you provide for the container instance must be unique on
@@ -16,7 +16,7 @@ filesystem.
 The specification file includes an args parameter. The args parameter is used
 to specify command(s) that get run when the container is started. To change the
 command(s) that get executed on start, edit the args parameter of the spec. See
-"runc spec --help" for more explanation.
+"docker-runc spec --help" for more explanation.
 
 # OPTIONS
    --bundle value, -b value  path to the root of the bundle directory, defaults to the current directory
diff --git a/man/runc-delete.8.md b/man/runc-delete.8.md
index 12ea711..ed7c49c 100644
--- a/man/runc-delete.8.md
+++ b/man/runc-delete.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc delete - delete any resources held by one or more containers often used with detached containers
+   docker-runc delete - delete any resources held by one or more containers often used with detached containers
 
 # SYNOPSIS
-   runc delete [command options] <container-id> [container-id...]
+   docker-runc delete [command options] <container-id> [container-id...]
 
 Where "<container-id>" is the name for the instance of the container.
 
@@ -10,8 +10,8 @@ Where "<container-id>" is the name for the instance of the container.
    --force, -f		Forcibly deletes the container if it is still running (uses SIGKILL)
 
 # EXAMPLE
-For example, if the container id is "ubuntu01" and runc list currently shows the
+For example, if the container id is "ubuntu01" and docker-runc list currently shows the
 status of "ubuntu01" as "stopped" the following will delete resources held for
-"ubuntu01" removing "ubuntu01" from the runc list of containers:  
+"ubuntu01" removing "ubuntu01" from the docker-runc list of containers:  
 
-       # runc delete ubuntu01
+       # docker-runc delete ubuntu01
diff --git a/man/runc-events.8.md b/man/runc-events.8.md
index 2f7bb0e..70d7a0d 100644
--- a/man/runc-events.8.md
+++ b/man/runc-events.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc events - display container events such as OOM notifications, cpu, memory, and IO usage statistics
+   docker-runc events - display container events such as OOM notifications, cpu, memory, and IO usage statistics
 
 # SYNOPSIS
-   runc events [command options] <container-id>
+   docker-runc events [command options] <container-id>
 
 Where "<container-id>" is the name for the instance of the container.
 
diff --git a/man/runc-exec.8.md b/man/runc-exec.8.md
index e47f828..49fa93a 100644
--- a/man/runc-exec.8.md
+++ b/man/runc-exec.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc exec - execute new process inside the container
+   docker-runc exec - execute new process inside the container
 
 # SYNOPSIS
-   runc exec [command options] <container-id> -- <container command> [args...]
+   docker-runc exec [command options] <container-id> -- <container command> [args...]
 
 Where "<container-id>" is the name for the instance of the container and
 "<container command>" is the command to be executed in the container.
@@ -11,7 +11,7 @@ Where "<container-id>" is the name for the instance of the container and
 For example, if the container is configured to run the linux ps command the
 following will output a list of processes running in the container:
 
-       # runc exec <container-id> ps
+       # docker-runc exec <container-id> ps
 
 # OPTIONS
    --console value              specify the pty slave path for use with the container
diff --git a/man/runc-kill.8.md b/man/runc-kill.8.md
index 3566e5f..fa1b0fd 100644
--- a/man/runc-kill.8.md
+++ b/man/runc-kill.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc kill - kill sends the specified signal (default: SIGTERM) to the container's init process
+   docker-runc kill - kill sends the specified signal (default: SIGTERM) to the container's init process
 
 # SYNOPSIS
-   runc kill [command options] <container-id> <signal>
+   docker-runc kill [command options] <container-id> <signal>
 
 Where "<container-id>" is the name for the instance of the container and
 "<signal>" is the signal to be sent to the init process.
@@ -15,4 +15,4 @@ Where "<container-id>" is the name for the instance of the container and
 For example, if the container id is "ubuntu01" the following will send a "KILL"
 signal to the init process of the "ubuntu01" container:
 
-       # runc kill ubuntu01 KILL
+       # docker-runc kill ubuntu01 KILL
diff --git a/man/runc-list.8.md b/man/runc-list.8.md
index f737424..cbb3433 100644
--- a/man/runc-list.8.md
+++ b/man/runc-list.8.md
@@ -1,18 +1,18 @@
 # NAME
-   runc list - lists containers started by runc with the given root
+   docker-runc list - lists containers started by runc with the given root
 
 # SYNOPSIS
-   runc list [command options]
+   docker-runc list [command options]
 
 # EXAMPLE
 Where the given root is specified via the global option "--root"
 (default: "/run/runc").
 
 To list containers created via the default "--root":
-       # runc list
+       # docker-runc list
 
 To list containers created using a non-default value for "--root":
-       # runc --root value list
+       # docker-runc --root value list
 
 # OPTIONS
    --format value, -f value     select one of: table or json (default: "table")
diff --git a/man/runc-pause.8.md b/man/runc-pause.8.md
index 489bb90..f899e76 100644
--- a/man/runc-pause.8.md
+++ b/man/runc-pause.8.md
@@ -1,12 +1,12 @@
 # NAME
-   runc pause - pause suspends all processes inside the container
+   docker-runc pause - pause suspends all processes inside the container
 
 # SYNOPSIS
-   runc pause <container-id> [container-id...]
+   docker-runc pause <container-id> [container-id...]
 
 Where "<container-id>" is the name for the instance of the container to be
 paused. 
 
 # DESCRIPTION
    The pause command suspends all processes in the instance of the container.
-Use runc list to identiy instances of containers and their current status.
+Use docker-runc list to identiy instances of containers and their current status.
diff --git a/man/runc-ps.8.md b/man/runc-ps.8.md
index d606347..fb89da2 100644
--- a/man/runc-ps.8.md
+++ b/man/runc-ps.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc ps - ps displays the processes running inside a container
+   docker-runc ps - ps displays the processes running inside a container
 
 # SYNOPSIS
-   runc ps [command options] <container-id> [-- ps options]
+   docker-runc ps [command options] <container-id> [-- ps options]
 
 # OPTIONS
    --format value, -f value     select one of: table(default) or json
@@ -10,4 +10,4 @@
 The default format is table.  The following will output the processes of a container
 in json format:
 
-    # runc ps -f json
+    # docker-runc ps -f json
diff --git a/man/runc-restore.8.md b/man/runc-restore.8.md
index 1793fb5..7c38242 100644
--- a/man/runc-restore.8.md
+++ b/man/runc-restore.8.md
@@ -1,15 +1,15 @@
 # NAME
-   runc restore - restore a container from a previous checkpoint
+   docker-runc restore - restore a container from a previous checkpoint
 
 # SYNOPSIS
-   runc restore [command options] <container-id>
+   docker-runc restore [command options] <container-id>
 
 Where "<container-id>" is the name for the instance of the container to be
 restored.
 
 # DESCRIPTION
    Restores the saved state of the container instance that was previously saved
-using the runc checkpoint command.
+using the docker-runc checkpoint command.
 
 # OPTIONS
    --image-path value           path to criu image files for restoring
diff --git a/man/runc-resume.8.md b/man/runc-resume.8.md
index d864a54..118d810 100644
--- a/man/runc-resume.8.md
+++ b/man/runc-resume.8.md
@@ -1,12 +1,12 @@
 # NAME
-   runc resume - resumes all processes that have been previously paused
+   docker-runc resume - resumes all processes that have been previously paused
 
 # SYNOPSIS
-   runc resume <container-id> [container-id...]
+   docker-runc resume <container-id> [container-id...]
 
 Where "<container-id>" is the name for the instance of the container to be
 resumed.
 
 # DESCRIPTION
    The resume command resumes all processes in the instance of the container.
-Use runc list to identiy instances of containers and their current status.
+Use docker-runc list to identiy instances of containers and their current status.
diff --git a/man/runc-run.8.md b/man/runc-run.8.md
index b5c6053..c714484 100644
--- a/man/runc-run.8.md
+++ b/man/runc-run.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc run - create and run a container
+   docker-runc run - create and run a container
 
 # SYNOPSIS
-   runc run [command options] <container-id>
+   docker-runc run [command options] <container-id>
 
 Where "<container-id>" is your name for the instance of the container that you
 are starting. The name you provide for the container instance must be unique on
@@ -16,7 +16,7 @@ filesystem.
 The specification file includes an args parameter. The args parameter is used
 to specify command(s) that get run when the container is started. To change the
 command(s) that get executed on start, edit the args parameter of the spec. See
-"runc spec --help" for more explanation.
+"docker-runc spec --help" for more explanation.
 
 # OPTIONS
    --bundle value, -b value  path to the root of the bundle directory, defaults to the current directory
diff --git a/man/runc-spec.8.md b/man/runc-spec.8.md
index f186139..7cfc793 100644
--- a/man/runc-spec.8.md
+++ b/man/runc-spec.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc spec - create a new specification file
+   docker-runc spec - create a new specification file
 
 # SYNOPSIS
-   runc spec [command options] [arguments...]
+   docker-runc spec [command options] [arguments...]
 
 # DESCRIPTION
    The spec command creates the new specification file named "config.json" for
@@ -27,9 +27,9 @@ command in a new hello-world container named container1:
     docker export $(docker create hello-world) > hello-world.tar
     mkdir rootfs
     tar -C rootfs -xf hello-world.tar
-    runc spec
+    docker-runc spec
     sed -i 's;"sh";"/hello";' config.json
-    runc start container1
+    docker-runc start container1
 
 In the start command above, "container1" is the name for the instance of the
 container that you are starting. The name you provide for the container instance
@@ -40,9 +40,9 @@ sub-command "ocitools generate" has lots of options that can be used to do any
 customizations as you want, see [ocitools](https://github.com/opencontainers/ocitools)
 to get more information.
 
-When starting a container through runc, runc needs root privilege. If not
-already running as root, you can use sudo to give runc root privilege. For
-example: "sudo runc start container1" will give runc root privilege to start the
+When starting a container through docker-runc, docker-runc needs root privilege. If not
+already running as root, you can use sudo to give docker-runc root privilege. For
+example: "sudo docker-runc start container1" will give docker-runc root privilege to start the
 container on your host.
 
 # OPTIONS
diff --git a/man/runc-start.8.md b/man/runc-start.8.md
index 6672120..137c637 100644
--- a/man/runc-start.8.md
+++ b/man/runc-start.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc start - start signals a created container to execute the user defined process
+   docker-runc start - start signals a created container to execute the user defined process
 
 # SYNOPSIS
-   runc start <container-id> [container-id...]
+   docker-runc start <container-id> [container-id...]
 
 Where "<container-id>" is your name for the instance of the container that you
 are starting. The name you provide for the container instance must be unique on
diff --git a/man/runc-state.8.md b/man/runc-state.8.md
index 80f4342..4120128 100644
--- a/man/runc-state.8.md
+++ b/man/runc-state.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc state - output the state of a container
+   docker-runc state - output the state of a container
 
 # SYNOPSIS
-   runc state <container-id>
+   docker-runc state <container-id>
 
 Where "<container-id>" is your name for the instance of the container.
 
diff --git a/man/runc-update.8.md b/man/runc-update.8.md
index 7f9c646..0f64bb4 100644
--- a/man/runc-update.8.md
+++ b/man/runc-update.8.md
@@ -1,8 +1,8 @@
 # NAME
-   runc update - update container resource constraints
+   docker-runc update - update container resource constraints
 
 # SYNOPSIS
-   runc update [command options] <container-id>
+   docker-runc update [command options] <container-id>
 
 # DESCRIPTION
    The data can be read from a file or the standard input, the
diff --git a/man/runc.8.md b/man/runc.8.md
index b5a8c54..3d80d8a 100644
--- a/man/runc.8.md
+++ b/man/runc.8.md
@@ -1,15 +1,15 @@
 # NAME
-   runc - Open Container Initiative runtime
+   docker-runc - Open Container Initiative runtime
 
 # SYNOPSIS
-   runc [global options] command [command options] [arguments...]
+   docker-runc [global options] command [command options] [arguments...]
    
 # DESCRIPTION
-runc is a command line client for running applications packaged according to
+docker-runc is a command line client for running applications packaged according to
 the Open Container Initiative (OCI) format and is a compliant implementation of the
 Open Container Initiative specification.
 
-runc integrates well with existing process supervisors to provide a production
+docker-runc integrates well with existing process supervisors to provide a production
 container runtime environment for applications. It can be used with your
 existing process monitoring tools and the container will be spawned as a
 direct child of the process supervisor.
@@ -20,7 +20,7 @@ The root filesystem contains the contents of the container.
 
 To start a new instance of a container:
 
-    # runc start [ -b bundle ] <container-id>
+    # docker-runc start [ -b bundle ] <container-id>
 
 Where "<container-id>" is your name for the instance of the container that you
 are starting. The name you provide for the container instance must be unique on
@@ -32,9 +32,9 @@ value for "bundle" is the current directory.
    delete       delete any resources held by the container often used with detached containers
    events       display container events such as OOM notifications, cpu, memory, IO and network stats
    exec         execute new process inside the container
-   init         initialize the namespaces and launch the process (do not call it outside of runc)
+   init         initialize the namespaces and launch the process (do not call it outside of docker-runc)
    kill         kill sends the specified signal (default: SIGTERM) to the container's init process
-   list         lists containers started by runc with the given root
+   list         lists containers started by docker-runc with the given root
    pause        pause suspends all processes inside the container
    ps           displays the processes running inside a container
    restore      restore a container from a previous checkpoint
-- 
2.11.0

